﻿using HeadFirstDesignPatterns.Models;
using HeadFirstDesignPatterns.Models.Flying;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadFirstDesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            Duck commonDuck = new Duck();
            commonDuck.Display();
            Console.WriteLine("====================================");

            MallardDuck duck = new MallardDuck();
            duck.Display();
            Console.WriteLine("====================================");

            ModelDuck modelDuck = new ModelDuck();
            modelDuck.PerformFly();
            modelDuck.SetFlyBehaviour(new FlyRocketPowered());
            modelDuck.PerformFly();
            Console.WriteLine("====================================");

            Console.ReadKey();
        }
    }
}
