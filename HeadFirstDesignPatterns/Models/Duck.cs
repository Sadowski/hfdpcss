﻿using HeadFirstDesignPatterns.Models.Flying;
using HeadFirstDesignPatterns.Models.Quacking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadFirstDesignPatterns.Models
{
    class Duck
    {
        public IFlyable FlyBehaviour;
        public IQuackable QuackBehaviour;

        public void SetFlyBehaviour(IFlyable fb)
        {
            FlyBehaviour = fb;
        }

        public void SetQuackBehaviour(IQuackable qb)
        {
            QuackBehaviour = qb;
        }

        public void PerformQuack()
        {
            QuackBehaviour.Quack();
        }

        public void PerformFly()
        {
            FlyBehaviour.Fly();
        }

        public void Swim()
        {
            Console.WriteLine("Swim, swim");
        }

        public virtual void Display()
        {
            Console.WriteLine("               __");
            Console.WriteLine("             <(o )___");
            Console.WriteLine("              ( ._> /");
            Console.WriteLine("               `---'   hjw");
        }


    }
}
