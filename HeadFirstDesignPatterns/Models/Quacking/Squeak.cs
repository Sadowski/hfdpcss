﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadFirstDesignPatterns.Models.Quacking
{
    class Squeak : IQuackable
    {
        public void Quack()
        {
            Console.WriteLine("SQUEEEK, SQUEEK");
        }
    }
}
