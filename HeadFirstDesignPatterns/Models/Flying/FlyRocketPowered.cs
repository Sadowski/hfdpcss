﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadFirstDesignPatterns.Models.Flying
{
    class FlyRocketPowered : IFlyable
    {
        public void Fly()
        {
            Console.WriteLine("WRRRRRRRRRRRRRUUUUUUUM, that's a rocket flying!!");
        }
    }
}
