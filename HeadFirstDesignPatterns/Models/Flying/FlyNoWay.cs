﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadFirstDesignPatterns.Models.Flying
{
    class FlyNoWay: IFlyable
    {
        public void Fly()
        {
            Console.WriteLine("Can't fly. Sorry");
        }
    }
}
