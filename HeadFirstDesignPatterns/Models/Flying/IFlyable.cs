﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadFirstDesignPatterns.Models.Flying
{
    interface IFlyable
    {
        void Fly();
    }
}
