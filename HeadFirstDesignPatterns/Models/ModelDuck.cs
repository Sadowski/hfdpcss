﻿using HeadFirstDesignPatterns.Models.Flying;
using HeadFirstDesignPatterns.Models.Quacking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadFirstDesignPatterns.Models
{
    class ModelDuck: Duck
    {
        public ModelDuck()
        {
            FlyBehaviour = new FlyNoWay();
            QuackBehaviour = new MuteQuack();
        }

        public override void Display()
        {
            Console.WriteLine("Just standing here... OR?!");
        }
    }
}
