﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherStation.Models.Interfaces;

namespace WeatherStation.Models.Displays
{
    class StatisticsDisplay : IObserver, IDisplayElement
    {
        private float maxTemp { get; set; } = 0.0f;
        private float minTemp { get; set; } = 200f;
        private float tempSum { get; set; } = 0.0f;
        private int numReadings { get; set; }

        public StatisticsDisplay(ISubject subject)
        {
            subject.RegisterObserver(this);
        }

        public void Display()
        {
            Console.WriteLine($"Avg/Max/Min temperature =  {tempSum / numReadings}/" +
                $"{maxTemp}/{minTemp}");
        }

        public void Update(float temp, float humidity, float pressure)
        {
            tempSum += temp;
            numReadings++;

            if (temp > maxTemp)
            {
                maxTemp = temp;
            }

            if (temp < minTemp)
            {
                minTemp = temp;
            }

            Display();
        }
    }
}
