﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherStation.Models.Interfaces;

namespace WeatherStation.Models.Displays
{
    class CurrentConditionsDisplay : IObserver, IDisplayElement
    {
        private float _temperature { get; set; }
        private float _humidity { get; set; }
        private ISubject _weatherData { get; set; }

        public CurrentConditionsDisplay( ISubject weatherData)
        {
            _weatherData = weatherData;
            _weatherData.RegisterObserver(this);
        }

        public void Update(float temp, float humidity, float pressure)
        {
            _temperature = temp;
            _humidity = humidity;
            Display();
        }

        public void Display()
        {
            Console.WriteLine($"Current conditions: {_temperature} F degrees and {_humidity} humidity");
        }
    }
}
