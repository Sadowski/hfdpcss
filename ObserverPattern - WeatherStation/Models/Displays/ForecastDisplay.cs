﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherStation.Models.Interfaces;

namespace WeatherStation.Models.Displays
{
    class ForecastDisplay: IObserver, IDisplayElement
    {
        private float currentPressure { get; set; } = 29.92f;
        private float lastPressure { get; set; }

        public ForecastDisplay(ISubject subject)
        {
            subject.RegisterObserver(this);
        }

        public void Update(float temp, float humidity, float pressure)
        {
            lastPressure = currentPressure;
            currentPressure = pressure;
            Display();
        }

        public void Display()
        {
            Console.Write("Forecast: ");
            if(currentPressure > lastPressure)
            {
                Console.Write("Improving weather on the way\n");
            }
            else if (currentPressure == lastPressure)
            {
                Console.Write("More of the same\n");
            }
            else if (currentPressure < lastPressure)
            {
                Console.Write("Watch out for cooler, rainy weather\n");
            }
        }
    }
}
