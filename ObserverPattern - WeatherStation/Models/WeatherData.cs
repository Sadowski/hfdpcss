﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherStation.Models.Interfaces;

namespace WeatherStation.Models
{
    class WeatherData : ISubject
    {
        private List<IObserver> observers { get; set; }
        private float temperature { get; set; }
        private float humidity { get; set; }
        private float pressure { get; set; }

        public WeatherData()
        {
            observers = new List<IObserver>();
        }

        public void NotifyObservers()
        {
            foreach(IObserver o in observers)
            {
                o.Update(temperature, humidity, pressure);
            }
        }

        public void RegisterObserver(IObserver o)
        {
            observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            int i = observers.IndexOf(o);
            if (i >= 0)
            {
                observers.RemoveAt(i);
            }
        }

        public void MeasurementsChanged()
        {
            NotifyObservers();
        }

        public void SetMeasurements(float temp, float hum, float pres)
        {
            temperature = temp;
            humidity = hum;
            pressure = pres;
            MeasurementsChanged();
        }
    }
}
